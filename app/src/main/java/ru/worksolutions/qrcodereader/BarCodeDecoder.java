package ru.worksolutions.qrcodereader;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;
import android.util.SparseArray;
import android.widget.Toast;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.podio.sdk.Podio;
import com.podio.sdk.Request;
import com.podio.sdk.domain.Item;

public class BarCodeDecoder {

    private static final String LOG_TAG = BarCodeDecoder.class.getSimpleName();

    private Context context;

    private int itemId;
    private String barCodeContents;

    BarCodeDecoder(Context context) {
        this.context = context;
    }

    public void decode(Bitmap bitmap){

        BarcodeDetector detector = new BarcodeDetector.Builder(context)
                .setBarcodeFormats(Barcode.DATA_MATRIX | Barcode.QR_CODE)
                .build();

        if(!detector.isOperational()){
            Log.e(LOG_TAG, "Could not set up the detector!");

            IntentFilter lowstorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
            boolean hasLowStorage = context.registerReceiver(null, lowstorageFilter) != null;

            if (hasLowStorage) {
                Toast.makeText(context, R.string.not_enough_disk_space, Toast.LENGTH_LONG).show();
                Log.e(LOG_TAG, context.getString(R.string.not_enough_disk_space));
            }
        }

        Frame frame = new Frame.Builder().setBitmap(bitmap).build();
        SparseArray<Barcode> barcodes = detector.detect(frame);

        if (barcodes.size() > 0) {
            Barcode thisCode = barcodes.valueAt(0);
            if (!thisCode.rawValue.equals(barCodeContents)) {
                barCodeContents = thisCode.rawValue;

                Log.e(LOG_TAG, barCodeContents);

                String[] barCodeContentsArray = barCodeContents.split("\n");
                for (String attribute : barCodeContentsArray) {
                    String[] attributeArray = attribute.split(": ");

                    if (attributeArray[0].equals("Item ID")) {
                        itemId = Integer.parseInt(attributeArray[1]);
                    }
                }

                Item item = new Item();
                item.addValue("filtered", 2);

                Podio.item.update(itemId, item)
                    .withErrorListener(new Request.ErrorListener() {
                        @Override
                        public boolean onErrorOccured(Throwable cause) {
                            Toast.makeText(context, "Unable to update item status", Toast.LENGTH_LONG).show();
                            Log.e(LOG_TAG, "Unable to update information on " + "ID " + String.valueOf(itemId) + " on server");
                            Log.e(LOG_TAG, cause.getMessage());
                            cause.printStackTrace();
                            return false;
                        }
                    });

                makeSound();
                Toast.makeText(context, "Scanned", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void makeSound(){
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(context, notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


