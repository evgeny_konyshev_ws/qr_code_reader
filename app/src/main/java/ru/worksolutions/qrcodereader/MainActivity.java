package ru.worksolutions.qrcodereader;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.WorkerThread;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.otaliastudios.cameraview.CameraView;
import com.otaliastudios.cameraview.Frame;
import com.otaliastudios.cameraview.FrameProcessor;
import com.otaliastudios.cameraview.Size;
import com.podio.sdk.Podio;
import com.podio.sdk.PodioError;
import com.podio.sdk.Request;

import java.io.ByteArrayOutputStream;

public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG = MainActivity.class.getSimpleName();
    private static final String APP_ID = "20939398";
    private static final String APP_TOKEN = "146ad0ff04214f1a8ec4901b6c9cee7f";

    private CameraView cameraView;
    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (!isNetworkConnected()){
            Toast.makeText(this, R.string.no_connection, Toast.LENGTH_LONG).show();
        }

        Podio.setup(this, getString(R.string.client_id), getString(R.string.client_secret));

        try {
            Podio.client.authenticateWithAppCredentials(APP_ID, APP_TOKEN)
                    .withSessionListener(new Request.SessionListener() {
                        @Override
                        public boolean onSessionChanged(String authToken, String refreshToken, long expires) {
                            Log.e(LOG_TAG, "Authentication succeded");
                            return false;
                        }
                    })
                .withErrorListener(new Request.ErrorListener() {

                    @Override
                    public boolean onErrorOccured(Throwable cause) {
                        Log.e(LOG_TAG, "Authentication failed");
                        return false;
                    }

                });


        final BarCodeDecoder barCodeDecoder = new BarCodeDecoder(this);

        cameraView = findViewById(R.id.camera);

        cameraView.addFrameProcessor(new FrameProcessor() {
            @Override
            @WorkerThread
            public void process(Frame frame) {
                byte[] data = frame.getData();
                bitmap = decodeToBitMap(data, frame);
                barCodeDecoder.decode(bitmap);
            }
        });

        }
        catch (PodioError e) {
            e.printStackTrace();
        }
    }

    public static Bitmap decodeToBitMap(byte[] data, Frame frame) {
        Size size = frame.getSize();
        try {
            YuvImage image = new YuvImage(data, ImageFormat.NV21, size.getWidth(), size.getHeight(), null);
            if (image != null) {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                image.compressToJpeg(new Rect(0, 0, size.getWidth(), size.getHeight()), 80, stream);
                Bitmap bmp = BitmapFactory.decodeByteArray(stream.toByteArray(), 0, stream.size());
                stream.close();
                return bmp;
            }
        } catch (Exception ex) {
        }
        return null;
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    protected void onResume() {
        super.onResume();
        cameraView.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        cameraView.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cameraView.destroy();
    }
}
